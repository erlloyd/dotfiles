source ~/.profile
source ~/.bashrc
export ENV=~/.bashrc

##
# Your previous /Users/lloyd/.bash_profile file was backed up as /Users/lloyd/.bash_profile.macports-saved_2013-03-11_at_10:31:01
##

# MacPorts Installer addition on 2013-03-11_at_10:31:01: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.


##
# Your previous /Users/lloyd/.bash_profile file was backed up as /Users/lloyd/.bash_profile.macports-saved_2014-04-29_at_11:32:00
##

# MacPorts Installer addition on 2014-04-29_at_11:32:00: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.


##
# Your previous /Users/lloyd/.bash_profile file was backed up as /Users/lloyd/.bash_profile.macports-saved_2014-12-10_at_13:11:18
##

# MacPorts Installer addition on 2014-12-10_at_13:11:18: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.


test -e ${HOME}/.iterm2_shell_integration.bash && source ${HOME}/.iterm2_shell_integration.bash
