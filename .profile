#export for LS color

export LSCOLORS=gxfxcxdxbxegedabagacad
export CLASSPATH=/Library/JOGL/lib/gluegen-rt.jar:/Library/JOGL/lib/jogl.jar:.:$AXIS_HOME/lib/axis2-adb-1.4.1.jar:$AXIS_HOME/lib/:~/Documents/EricDocuments/workspace/SWE622_FERS/lib/log4j-1.2.15.jar
#alias

alias ls='ls -lG'
alias xterm='xterm -sl 2048 -bg black -fg green'
alias ws='cd ~/Documents/EricDocuments/workspace'
alias mets='cd ~/Documents/EricDocuments/workspace642/SWE642_METS'
alias fers='cd ~/Documents/EricDocuments/workspace/SWE622_FERS'
alias hsqldbutil='/Library/hsqldb/bin/runUtil.sh DatabaseManagerSwing'

alias cs652demo='java cs652.elloyd1.project.EricLloyd_FinalProject'

alias aws_jenkins='ssh -i ~/.ssh/erlloyd-key-pair.pem ec2-user@ec2-18-220-210-218.us-east-2.compute.amazonaws.com'

alias docker_container_clear='docker rm -f $(docker ps -aq)'

export PATH=$PATH:~/bin

#exports for Apache Tomcat 6.0
#export JAVA_HOME=/Library/Java/Home
export JAVA_HOME="/Library/Java/JavaVirtualMachines/jdk1.8.0_144.jdk/Contents/Home"
export MAVEN_HOME="$HOME/dev/apache-maven-3.5.0"
export CATALINA_HOME=/Library/Tomcat/Home
export GRAILS_HOME=/Library/Grails/Home
export AXIS_HOME=/Library/Axis2/Home
export ANDROID_HOME=/Library/android/Home
export ANDROID_NDK_HOME=/Library/android/NDK
export PATH=$JAVA_HOME/bin:$PATH:$MAVEN_HOME/bin:$AXIS_HOME/bin:$GRAILS_HOME/bin:$ANDROID_HOME:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$ANDROID_NDK_HOME
export AXIS2_DEPLOY=/Library/Tomcat/Home/webapps/axis2/WEB-INF/services/
export LOG4J_HOME=/Library/Java/log4j/Home


export NVM_DIR="$HOME/.nvm"
#. "/usr/local/opt/nvm/nvm.sh"
