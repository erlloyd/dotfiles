
#NOTE: Anything you want to go in both bash and zsh should go in ~/.profile, which
#      is sourced by both bash and zsh
export PS1='\u@\h:\W \$ '

