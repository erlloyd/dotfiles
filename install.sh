#!/bin/bash

#first, need to do this so vim works
git config --global core.editor /usr/bin/vim

#now, make symlinks to all the files we just cloned
DOTFILES_BAK=$HOME'/.dotfiles_bak'
THIS_DIR=`pwd`

mkdir $DOTFILES_BAK 1>/dev/null 2>&1

FILE_TO_LINK=( '.ackrc' '.bash_completion' '.bash_completion.d' '.profile' '.zshrc' '.bash_profile' '.bashrc' '.dircolors' '.gitconfig' '.gitignore' '.gitignore_global' '.gitmodules' '.inputrc' '.mintty' '.screenrc' '.toprc' '.vim' '.vimrc' )

for file in "${FILE_TO_LINK[@]}"
do
    echo "Installing $file..."

    if [ -f $DOTFILES_BAK/$file ] || [ -d $DOTFILES_BAK/$file ]
    then
        echo ".......backup exists. Removing current file"
        rm ~/$file
    else
        echo ".......no backup exists. Moving file if it is there"

        mv $HOME/$file $DOTFILES_BAK 1>/dev/null 2>&1
    fi

    echo ".......creating link to $THIS_DIR/$file"
    ln -s $THIS_DIR/$file $HOME/$file

    echo ""
done

#mv ~/.bash_comp $DOTFILES_BAK
#ln -s $THIS_DIR/.ackrc ~/.ackrc
echo "Completed installation"

